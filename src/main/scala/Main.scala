import java.net.URLEncoder
import java.util.Properties

import scala.collection.JavaConverters._
import SlickTables.Tables.{GfbioSourceRow, SourceKeywordsRow}
import slick.basic.{DatabaseConfig, DatabasePublisher}
import slick.driver.JdbcProfile
import slick.lifted.{Aliases, Query, TableQuery}

import scala.util.parsing.json
import scala.util.parsing.json.JSON
import java.sql.{Connection, DriverManager, ResultSet}
import java.util

import com.twitter.finagle.http.Response
import edu.stanford.nlp.ling.CoreAnnotations.{SentencesAnnotation, TextAnnotation}
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation

import scala.collection.mutable.ListBuffer


// CoreNLP for NLP
import scala.concurrent.ExecutionContext.Implicits.global
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations
import edu.stanford.nlp.pipeline._
import edu.stanford.nlp.ling.{CoreAnnotations, CoreLabel, Word}
import edu.stanford.nlp.trees.{CollinsHeadFinder, SemanticHeadFinder, Tree, TreeCoreAnnotations}
import edu.stanford.nlp.util.CoreMap

// Sclick for connection to DB
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.JdbcBackend.Database
import slick.dbio.DBIO
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

// auto generated slick tables
import SlickTables.Tables

object Main {

  // initialize coreNLP
  val props = new Properties()
  props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse")
  val pipeline = new StanfordCoreNLP(props)

  def main(args: Array[String]): Unit = {
    import scala.concurrent.Await
    import scala.concurrent.duration.Duration
    import scala.util.{Failure, Success, Try}
    println("initialization")

    // Connect to postgres

    val driver = "org.postgresql.Driver"
    val dbUrl = "jdbc:postgresql://localhost:5432/gfbio"
    val username = "postgres"
    val password = "postgres" // do not do this in production
    val db = Database.forConfig("postgresDB")

    /* Fill the source_keywords table */

    def processSourceTable(): Unit = {
      // SELECT * FROM gfbio_source
      val selectSource: Future[Seq[Tables.GfbioSourceRow]] = db.run(Tables.GfbioSource.result)

      /* block thread until result is complete */
      val sourceTable: Try[Seq[GfbioSourceRow]] = Await.ready(selectSource, Duration.Inf).value.get

      sourceTable match {
        case Success(table) =>
          for (row <- table) {
            val sourceString = row.sourcestring.get
            val titleNPs = findNPs(sourceString)
            val tfmap = countTF(titleNPs)
            for (nptf <- tfmap) {
              val rowInsert = DBIO.seq(Tables.SourceKeywords += Tables.SourceKeywordsRow(row.sourceid, nptf._1, nptf._2))
              val f = db.run(rowInsert)
              Await.ready(f, Duration.Inf)
            }
          }
        case Failure(t) => println(t)
        case _ => println("unexpected, since Try[T] may only have 2 cases")
      }
    }

    processSourceTable()

    val a = for {
      uids <- Tables.GfbioSource.map(row => row.userid).distinct
      sid <- Tables.GfbioSource join Tables.SourceKeywords on (_.sourceid === _.sourceid) map {row => (row._1.userid, (row._1.sourceid, row._2.keyword, row._2.termfrequency))}
    } yield sid

    val r = Await.ready(db.run(a.result), Duration.Inf).value.get

    r match {
      case Success(uidToSidList) =>
        // uidToSidMap = (userid, (sourceid, keyword, tf))
        val uidToSidMap = uidToSidList.groupBy(_._1) map {case (k,v) => (k, v.map(_._2))}
        for (user <- uidToSidMap) {
          val data = user._2
          val documents = data.groupBy(_._1) map {case (sid, rest) => (sid, rest.map(x => (x._2, x._3)).distinct)}
          // find all distinct keywords
          val keywords = data.map(p => p._2).distinct

          // for every keyword compute tf idf
          for (keyword <- keywords) {
            val tf = documents.values.flatten.filter(x => x._1 == keyword).map(_._2).sum
            val df = documents.count(x => x._2.map(y => y._1).contains(keyword))
            val iduf = Math.log(documents.keySet.size.toDouble / df)
            val tfiduf = tf * iduf
            println(s"keyword $keyword + TFIDF $tfiduf")
            updateUserInterests(user._1, keyword, tfiduf)

          }
        }
      case Failure(t) => println(t)
    }



    // refactor this, bad way of working with resources
    //    try {
    //      Class.forName(driver)
    //      val connection = DriverManager.getConnection(dbUrl, username, password)
    //      val statement = connection.createStatement
    //      val rs = statement.executeQuery("SELECT * FROM gfbio_source")
    //      while (rs.next) {
    //        val sourceID = rs.getString("sourceid")
    //        val userid = rs.getString("userid")
    //        val sourcestring = rs.getString("sourcestring")
    //        val mapNPsTF = countTF(findNPs(sourcestring))
    //        // write content of this map to source_keywords table
    //        //mapNPsTF
    //        println("userid = %s, sourcestring = %s".format(userid, sourcestring))
    //        connection.close()
    //      }
    //    } catch {
    //      case e: Throwable => e.printStackTrace()
    //    }
  }

  /*
  * For debug purposes
  * */
  private def printSyntaxTree(text: String) = {
    val a = pipeline.process(text)
    val s = a.get(classOf[SentencesAnnotation])
    val t = s.get(0).get(classOf[TreeAnnotation])
    t.pennPrint()
  }

  def findNPs(title: String): List[String] = {
    // run pipelne to annotate title with selected annotations (see constructor)
    val annotation = pipeline.process(title)

    // get sentences
    val sentences: util.List[CoreMap] = annotation.get(classOf[CoreAnnotations.SentencesAnnotation])

    // construct syntax tree for every sentence
    var listNPs = List[String]()
    for (sentence <- sentences.asScala) {
      val tree = sentence.get(classOf[TreeCoreAnnotations.TreeAnnotation])
      if (tree == null) println("no tree")
      else {
        // DFS tree to find minimal NPs
        listNPs ++= dfsNPfinder(tree)
      }
    }

    return listNPs
  }


  def dfsNPfinder(tree: Tree): List[String] = {
    if (tree.value().equals("NP")) {
      // if current node is NP, check if it is minimal. If yes, return current node, else return minimal node of the subtree
      // else check all child node, if some of them are NPs

      if (tree.isLeaf) {
        val singleNPList: ListBuffer[String] = ListBuffer[String]()
        singleNPList += getNPString(tree.getLeaves.toArray)
        return singleNPList.toList
      } else {
        val childrenNPList: ListBuffer[String] = ListBuffer[String]()
        for (node <- tree.children) {
          childrenNPList ++= dfsNPfinder(node)
        }
        if (childrenNPList.isEmpty) {
          val singleNPList: ListBuffer[String] = ListBuffer[String]()
          singleNPList += getNPString(tree.getLeaves.toArray)
          return singleNPList.toList
        } else return childrenNPList.toList
      }
    }
    else {
      if (tree.isLeaf) {
        return List[String]()
      } else {
        val listNP = ListBuffer[String]()
        tree.children().foreach((childNode: Tree) => listNP ++= dfsNPfinder(childNode))
        return listNP.toList
      }
    }

  }

  def getNPString(leafs: Array[AnyRef]): String = {
    var s: StringBuilder = StringBuilder.newBuilder

    for (leaf <- leafs) {
      s.append(leaf.toString)
      s.append(" ")
    }
    return s.dropRight(1).toString().toLowerCase()
  }

  def countTF(listRawNPs: List[String]): Map[String,Int] = {
    return listRawNPs.groupBy((l: String) => l).map((t: (String, List[String])) => (t._1, t._2.length))
  }

  /* for debug */
  def printNPs(tree: Tree): Unit = {
    if (tree == null || tree.isLeaf) return
    if (tree.value().equals("NP")) println("NP: " + tree.getLeaves)
    tree.children().toStream.foreach((leaf: Tree) => printNPs(leaf))
  }

  def updateUserInterests(userid: Long, interest: String, interestWeight: Double): Unit = {
    import com.twitter.finagle.{Http, Service, http}
    import com.twitter.util.{Await, Future, Base64StringEncoder => Base64}

    val serviceURL = "127.0.0.1:8080"
    val serviceName = "GFBio-Search-Service-portlet.user_interests/"
    val resourceType = "update-user-interests/"
    val requestURL = serviceName + resourceType + "interests-id/0/user-id/" + userid + "/interest-string/" + interest.replaceAll(" ", "%20") + "/interest-weight/" + interestWeight
    val basicAuth = "Basic " + Base64.encode("test@liferay.com:test".getBytes("UTF-8"))

    val client: Service[http.Request, http.Response] = Http.newService(serviceURL)
    val request = http.Request(http.Version.Http11, http.Method.Post, requestURL)
    request.host = "127.0.0.1"
    request.headerMap ++= Seq("Authorization" -> basicAuth, "Accept" -> "application/json")

    println("Request: \n" + request.encodeString())

    val response: com.twitter.util.Future[http.Response] = client(request)
    com.twitter.util.Await.result(response.onSuccess{rep: http.Response =>
      println("Response: \n" + rep.encodeString())})
  }

//  def getSourceById(sourceID: String): Unit = {
//    // finagle for JSONWS
//    import com.twitter.finagle.{Http, Service, http}
//    import com.twitter.util.{Await, Future, Base64StringEncoder => Base64}
//
//    val serviceURL = "127.0.0.1:8080"
//    val serviceName = "/api/jsonws/GFBio-Search-Service-portlet.source/get-source-id-by-source-type/"
//    val resourceType = "sourceType"
//    val basicAuth = "Basic " + Base64.encode("test@liferay.com:test".getBytes("UTF-8"))
//
//    val client: Service[http.Request, http.Response] = Http.newService(serviceURL)
//    val request = http.Request(http.Method.Get, serviceName + "/" + resourceType + "/" + sourceID)
//    request.host = "127.0.0.1"
//    request.headerMap ++= Seq("Authorization" -> basicAuth, "Accept" -> "application/json")
//
//    println("Request: \n" + request.encodeString())
//    val response: Future[http.Response] = client(request)
//
//    Await.result(response.onSuccess(rep: http.Response =>
//      println("Response: \n" + rep.encodeString())))
//  }
//
//  def updateSource(sourceID: Long, userID: Long, sourceType: String,
//                   sourceString: String, sourceFilter: String): Unit = {
//    // finagle for JSONWS
//    import com.twitter.finagle.{Http, Service, http}
//    import com.twitter.util.{Await, Future, Base64StringEncoder => Base64}
//
//    val serviceURL = "localhost:8080"
//    val serviceName = "/api/jsonws/GFBio-Search-Service-portlet.source/update-source/"
//    val resourceType = "sourceId"
//    val basicAuth = "Basic " + Base64.encode("test@liferay.com:test".getBytes("UTF-8"))
//
//    val client: Service[http.Request, http.Response] = Http.newService(serviceURL)
//    val request = http.Request(http.Method.Get, "http://localhost:8080/api/jsonws/GFBio-Search-Service-portlet.user_interests/update-user-interests/interests-id/0/user-id/20198/interest-string/quercus/interest-weight/0.5")
//    request.host = "localhost"
//    request.headerMap ++= Seq("Authorization" -> basicAuth, "Accept" -> "application/json;charset=UTF-8")
//    request.setContentTypeJson()
//    println("Request: \n" + request.encodeString())
//    val response: Future[http.Response] = client(request)
//
//    Await.result(response.onSuccess(rep: http.Response =>
//      println("Response: \n" + rep.encodeString())))
//  }


}