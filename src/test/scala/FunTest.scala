import org.scalatest.FunSuite
import Main.{findNPs, countTF}

class FunTest extends FunSuite{
  test("Function findNPs with the argument 'Big green cat is climbing on the tree.' should return List with elements ['big green cat, 'the tree']") {
    assert(List[String]("big green cat", "the tree") == findNPs("Big green cat is climbing on the tree."))
  }

  test("CountTF function with argument List('tree', 'tree', 'tree', 'cat', 'cat') will return a Map('tree'->3, 'cat'->2)") {
    val expected = Map("tree"->3, "cat"->2)
    val actual = countTF(List("tree","tree","tree","cat","cat"))
    assert(expected == actual)
  }
}
